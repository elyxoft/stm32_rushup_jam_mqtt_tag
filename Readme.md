# STM32_RUSHUP_JAM_MQTT_TAG

Look at http://www.elyxoft.fr for usage.

## Purpose

use a RUSHUP Cloud-JAM board (NUCLEO-F401RE + X-NUCLEO-IDW01M1 + X-NUCLEO-IKS01A2 + X-NUCLEO-NFC01A1) as a simple tag who report temperature, humidity and pressure every 5 minutes to a broker mqtt.  

## Documents  

https://www.rushup.tech/jam/  
https://github.com/rushup/Cloud-JAM  
https://www.st.com/en/evaluation-tools/p-nucleo-cld1.html  
https://github.com/rushup/Cloud-JAM/wiki/Getting-started-with-Cloud-JAM  
