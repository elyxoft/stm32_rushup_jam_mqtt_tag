   /**
  ******************************************************************************
  * @file    IBM_Watson_Config.h
  * @author  Central LAB
  * @version V1.0.0
  * @date    17-Oct-2015
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2015 STMicroelectronics</center></h2>
  * 
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */


#ifndef __IBM_Watson_Config_H
#define __IBM_Watson_Config_H

#include "stdio.h"
#include "stdint.h"
#include "MQTTClient.h"

/* IKS x-cube related section    ---------------------------------------------------*/
#include "x_nucleo_iks01a2.h"
#include "x_nucleo_iks01a2_humidity.h"
#include "x_nucleo_iks01a2_temperature.h"
#include "x_nucleo_iks01a2_pressure.h"

/** USER WIFI configure SSID and KEY. */
#define CUSTOM_WIFI_SSI "raspi-webgui" // "SebNetgear"
#define CUSTOM_WIFI_KEY "ChangeMe" // "Tu2ser8v"

/** USER MQTT configure. */
#define CUSTOM_MQTT_BROKER_ADDRESS	"10.3.141.1" // "192.168.1.73"
#define CUSTOM_MQTT_BROKER_TOPIC	"stm32"


/* Add versioning information */
#define JAM_MQQT_TAG_DEMO_APP_VERSION_MAJOR (1)
#define JAM_MQQT_TAG_DEMO_APP_VERSION_MINOR (0)
#define JAM_MQQT_TAG_DEMO_APP_VERSION_MICRO (0)

#define MAX_CLIENT_ID_LEN (100)

/* define sensors depending on whether we are using X-NUCLEO-IKS01A2 */
#define MAGNETOMETER_SENSOR_AUTO (LSM303AGR_M_0)

enum { C_ACCEPTED = 0, C_REFUSED_PROT_V = 1, C_REFUSED_ID = 2, C_REFUSED_SERVER = 3, C_REFUSED_UID_PWD = 4, C_REFUSED_UNAUTHORIZED = 5 };

/**
  * @brief  Parameters for MQTT connection
  */
typedef struct mqtt_vars
{
	uint8_t pub_topic[128];  /**< topic to publish */
    enum QoS qos;            /**< Quality of service parameter for MQTT connection to IBM Watson IOT platform service */
	uint8_t hostname[64];    /**< your_ibm_org_id."messaging.internetofthings.ibmcloud.com"  */
	uint32_t port;                 /**< TCP port */
	uint8_t protocol; /**<t -> tcp , s-> secure tcp, c-> secure tcp + certificates */
} MQTT_vars;


/**
  * @brief  This structure contains all the parameters which user can pass to firmware
  */
typedef struct user_vars
{
        char NetworkSSID[32];         /**< SSID of the WIFI network that your using for internet connectivity.  */
        char AuthenticationType[6];   /**< Authentication type for Wifi (e.g. WEP/WPA2 etc.) */
        char NetworkKey[32];          /**< Wifi security key corresponding to your Wifi SSID */
        enum QoS qos;                 /**< Quality of service parameter for MQTT connection to IBM Watson IOT platform service */
} User_vars;

#define UNION_DATA_SIZE_TEXT (32*2+6+sizeof(enum QoS))

/**
  * @brief  This union contains all the parameters which user can pass to firmware
  */
typedef union
{
  uint32_t Data[UNION_DATA_SIZE_TEXT];            /**< : data buffer */
  User_vars configParams;                     /**< : structure for holding user configuration parameters */
}uNfcTokenInfo;

extern uNfcTokenInfo UnionNFCToken;

/* MQTT Functions */
void Config_MQTT ( MQTT_vars *mqtt_ibm_setup, uint8_t *macadd );


#endif 


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
