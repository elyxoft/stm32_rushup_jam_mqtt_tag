   /**
  ******************************************************************************
  * @file    main.c
  * @author  Central LAB, MP
  * @version V2.0.0
  * @date    11-November-2016
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2015 STMicroelectronics</center></h2>
  * 
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stdio.h"
#include "ctype.h"
#include "string.h"
#include "math.h"
#include "Config_MQTT.h"
#include "stm32f4xx_I2C.h"
#include "stm32f4xx_periph_conf.h"
#include "lib_TagType4.h"
#include "lib_NDEF_Text.h"

/**
   * @mainpage FP-CLD-WATSON1  firmware package
   * <b>Introduction</b> <br>
   * FP-CLD-WATSON1 provides a software running on STM32 which offers a complete middleware to build applications based on Wi-Fi connectivity (SPW01SA) and to connect STM32 Nucleo boards 
   * with MQTT Broker. This firmware package includes Components Device Drivers, Board Support Package and example application for STMicroelectronics X-NUCLEO-IDW01M1
   * X-NUCLEO-NFC01A1, X-NUCLEO-IKS01A1.

   * <b>Toolchain Support</b><br>
   * The library has been developed and tested on following toolchains:
   *        - System Workbench for STM32 (SW4STM32) V1.2 + ST-LINK
*/


/** @addtogroup FP-CLD-WATSON1
  * @{
  */

/* Extern () ------------------------------------------------------------*/
extern WiFi_Status_t GET_Configuration_Value(char* sVar_name,uint32_t *aValue);

/* Private define ------------------------------------------------------------*/
#define WIFI_SCAN_BUFFER_LIST           32
#define WIFI_SCAN_BUFFER_SIZE           512
#define MQTT_BUFFER_SIZE                512
#define MQTT_PUBLISH_DELAY              60000 // delay between two publish in ms
// Timeout for setting wifi parameters 
#define APP_TIMEOUT 5000
#define NDEF_WIFI 32
#define BLUEMSYS_FLASH_ADD ((uint32_t)0x08060000)
#define BLUEMSYS_FLASH_SECTOR FLASH_SECTOR_7
#define WIFI_CHECK_SSID_KEY ((uint32_t)0x12345678)
#define UNION_DATA_SIZE ((32+32+6+6)>>2)
#define USART_SPEED_MSG	460800

uNfcTokenInfo UnionNFCToken;
static System_Status_t status = MODULE_SUCCESS;


/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void prepare_json_pkt (uint8_t * buffer);
static System_Status_t  Get_MAC_Add (char *macadd);
static System_Status_t InitNucleo(int i_usart_speed);
static System_Status_t InitSensors(void);
static System_Status_t ConfigWiFi(void);
static System_Status_t InitMQTT(void);
static void setDefaultConfig();
static void ShowHwConfiguration();
int progFunctionInit(void);

static void NotifyLEDOn();
static void NotifyLEDOff(void);
static void NotifyLEDBlink(unsigned int msPeriod);


/* WiFi. Private variables ---------------------------------------------------------*/  
typedef enum {
  wifi_state_reset = 0,
  wifi_state_ready,
  wifi_state_idle,
  wifi_state_connected,
  wifi_state_connecting,
  wifi_state_disconnected,
  wifi_state_error,
  wifi_state_socket_close,
  wifi_state_reconnect,
  mqtt_socket_create,
  client_conn,
  mqtt_connect,
  mqtt_pub,
  mqtt_device_control,
  wifi_undefine_state       = 0xFF,  
} wifi_state_t;

wifi_state_t wifi_state;
wifi_config config;
wifi_scan net_scan[WIFI_SCAN_BUFFER_LIST];
uint8_t console_input[1], console_count=0;
wifi_bool set_AP_config = WIFI_FALSE, SSID_found = WIFI_FALSE;
uint8_t json_buffer[MQTT_BUFFER_SIZE];

/* Default configuration SSID/PWD */
char *ssid = CUSTOM_WIFI_SSI;
char *seckey = CUSTOM_WIFI_KEY;
  
WiFi_Priv_Mode mode = WPA_Personal; 

/* MQTT. Private variables ---------------------------------------------------------*/  
unsigned char MQTT_read_buf[MQTT_BUFFER_SIZE];
unsigned char MQTT_write_buf[MQTT_BUFFER_SIZE];
Network  n;
Client  c;
MQTTMessage  MQTT_msg;
MQTT_vars mqtt_ibm_setup;
System_Status_t nfcStatus = MODULE_ERROR;
  
/* Timer for MQTT */
static System_Status_t MQTTtimer_init(void);
TIM_HandleTypeDef        MQTTtimHandle;
TIM_IC_InitTypeDef       sConfig;
   
void TIM4_IRQHandler(void);

/* Sensors. Private variables ---------------------------------------------------------*/  
void *ACCELERO_handle = NULL;
void *GYRO_handle = NULL;
void *MAGNETO_handle = NULL;
void *HUMIDITY_handle = NULL;
void *TEMPERATURE_handle = NULL;
void *PRESSURE_handle = NULL;
#define MCR_BLUEMS_F2I_1D(in, out_int, out_dec) {out_int = (int32_t)in; out_dec= (int32_t)((in-out_int)*10);};
#define MCR_BLUEMS_F2I_2D(in, out_int, out_dec) {out_int = (int32_t)in; out_dec= (int32_t)((in-out_int)*100);};
 
volatile uint8_t  AXL_DRDY_received = 0;
volatile uint32_t Int_Current_Time1 = 0;
volatile uint32_t Int_Current_Time2 = 0;

/**
  * @brief  Main program
  * 
  * @retval None
  */
int main(void) {

  WiFi_Status_t status_spwf = WiFi_MODULE_SUCCESS;
  int8_t ret_code;
  uint32_t i;
    
   /* Init Nucleo Board */
  if (InitNucleo (USART_SPEED_MSG) !=  MODULE_SUCCESS ) {  
    printf("\n\rFailed to Initialize Nucleo board \n\r"); 
    return 0; 
  } else {
    printf("\n\rNucleo board initialized."); 
  }
  
  HAL_Delay(1000);
  printf("\033[2J\033[0;0H******************************************************************************\r\n");  
  printf("\n\r**************************************************************************\r\n");
  printf("***                JAM-Cloud MQTT Tag                                  ***\r\n");
  printf("**************************************************************************\n\r");
  
  printf("Firmware version: %d.%d.%d\n\r", JAM_MQQT_TAG_DEMO_APP_VERSION_MAJOR, JAM_MQQT_TAG_DEMO_APP_VERSION_MINOR, JAM_MQQT_TAG_DEMO_APP_VERSION_MICRO);
  fflush(stdout);
  
  /* Not use NFC expansion board*/
  nfcStatus = MODULE_NFC_NOT_USED;
  
  ShowHwConfiguration();
  
  HAL_Delay(1000);
    
  /* Init sensors expansion board */
  if ((status=InitSensors()) < 0 ) {  
    printf("Failed to Initialize Sensors board.\r\n"); 
    return status; 
  } else {
    printf("Sensors board initialized.\r\n");   
  }
  
  HAL_Delay(1000);  

  if( !progFunctionInit() ){
    printf("\033[2J\033[0;0HError during program initialization...\r\nPlease restart nucleo board.\r\n");
    while(1);
  }
  
  while (1)
  {
	switch (wifi_state)
	{
	case wifi_state_reconnect:
		  printf("\r\n [E] WiFi connection lost. Wait 10 sec then reconnect with parameters:  \r\n");
		  printf((char *)UnionNFCToken.configParams.NetworkSSID);
		  printf("\r\n");
		  printf((char *)UnionNFCToken.configParams.NetworkKey);
		  printf("\r\n");
		  HAL_Delay(10000);
		  status_spwf = wifi_connect(UnionNFCToken.configParams.NetworkSSID, UnionNFCToken.configParams.NetworkKey, mode);
		  if(status_spwf!=WiFi_MODULE_SUCCESS){
			 printf("\r\n[E].Error cannot connect with WiFi \r\n");
			 wifi_state = wifi_state_reconnect;
		  } else{
			   printf("\r\n [D] Reconnecting....  \r\n");
			   printf((char *)UnionNFCToken.configParams.NetworkSSID);
			   wifi_state = wifi_state_idle;
		  }
		  break;

	 case wifi_state_reset:
		break;

	 case wifi_state_ready:
	   HAL_Delay(20);
		status_spwf = wifi_network_scan(net_scan, WIFI_SCAN_BUFFER_LIST);
		if(status_spwf==WiFi_MODULE_SUCCESS)
		{
		  if (strcmp(UnionNFCToken.configParams.AuthenticationType, "NONE") == 0)
			   mode = None;
		  else if (strcmp(UnionNFCToken.configParams.AuthenticationType, "WEP") == 0)
			   mode = WEP;
		  else
			   mode = WPA_Personal;

		  for ( i=0; i<WIFI_SCAN_BUFFER_LIST; i++ )
			{
				if(( (char *) strstr((const char *)net_scan[i].ssid,(const char *)UnionNFCToken.configParams.NetworkSSID)) !=NULL) {
				   SSID_found = WIFI_TRUE;
				   memcpy(UnionNFCToken.configParams.AuthenticationType, "WPA2", strlen("WPA2"));
				   status_spwf = wifi_connect(UnionNFCToken.configParams.NetworkSSID, UnionNFCToken.configParams.NetworkKey, mode);
				   
				   if(status_spwf!=WiFi_MODULE_SUCCESS) {
					  printf("\r\n[E].Error cannot connect to WiFi network \r\n");
					  return 0;
				   }else{
					  printf("\r\n [D] Connecting to network with SSID ");
					  printf((char *)UnionNFCToken.configParams.NetworkSSID);
				   }
				   break;
				 }
			}
		  
			if(!SSID_found)
			{
				   /* Can happen in crowdy environments */
				   printf("\r\n[E]. Error, given SSID not found! Trying to force connection with SSID: \r\n");
				   printf((char *)UnionNFCToken.configParams.NetworkSSID);
				   status_spwf = wifi_connect(UnionNFCToken.configParams.NetworkSSID, UnionNFCToken.configParams.NetworkKey, mode);
				   if(status_spwf!=WiFi_MODULE_SUCCESS){
					  printf("\r\n[E].Error cannot connect with WiFi \r\n");
					  return 0;
				   } else{
						printf("\r\n [D] Connected to network with SSID  \r\n");
						printf((char *)UnionNFCToken.configParams.NetworkSSID);
				   }

			}
			memset(net_scan, 0x00, sizeof(net_scan));
		}
		else
			printf("\r\n[E]. Error, network AP not found! \r\n");

		wifi_state = wifi_state_idle;
		break;

	case wifi_state_connected:
		// Low power mode not used
		break;

	case mqtt_socket_create:
	  if(spwf_socket_create (&n,mqtt_ibm_setup.hostname, mqtt_ibm_setup.port, &mqtt_ibm_setup.protocol)<0){
		  printf("\r\n [E]. Socket opening with MQTT broker failed. Please check MQTT configuration. Trying reconnection.... \r\n");
		  printf((char*)mqtt_ibm_setup.hostname);
		  printf("\r\n");
		  printf((char*)(&mqtt_ibm_setup.protocol));
		  printf("\r\n");
		  wifi_state=mqtt_socket_create;
		  HAL_Delay(2000);
	  }else{
		  /* Initialize MQTT client structure */
		  MQTTClient(&c,&n, 4000, MQTT_write_buf, sizeof(MQTT_write_buf), MQTT_read_buf, sizeof(MQTT_read_buf));
		  wifi_state=mqtt_connect;

		  printf("\r\n [D]. Created socket with MQTT broker. \r\n");
	  }
	  break;

	case mqtt_connect:

	  if ( (ret_code = MQTTConnect(&c, NULL)) != 0){
		  printf("\r\n [E]. Client connection with MQTT broker failed with Error Code %d \r\n", (int)ret_code);
		  switch (ret_code)
		  {
				case C_REFUSED_PROT_V:
				  printf("Connection Refused, unacceptable protocol version \r\n");
				  break;
				case C_REFUSED_ID:
				  printf("Connection Refused, identifier rejected \r\n");
				  break;
				case C_REFUSED_SERVER:
				  printf("Connection Refused, Server unavailable \r\n");
				  break;
				case C_REFUSED_UID_PWD:
				  printf("Connection Refused, bad user name or password \r\n");
				  break;
				case C_REFUSED_UNAUTHORIZED:
				  printf("Connection Refused, not authorized \r\n");
				  break;
		  }
		  spwf_socket_close (&n);
		  wifi_state = mqtt_socket_create;
		  HAL_Delay(2000);
		 /* Turn LED2 Off to indicate that we are now disconnected from MQTT broker */
		  NotifyLEDOff();
	  }else {
		 printf("\r\n [D]. Connected with MQTT broker, only MQTT publish supported \r\n");
		 /* We only publish data. */
		 wifi_state = mqtt_pub;
		 /* Turn LED2 ON to indicate that we are now connected to MQTT broker */
		 NotifyLEDOn();
	  }
	  HAL_Delay(1500);
	 break;

	case mqtt_pub:

	  prepare_json_pkt(json_buffer);

	  MQTT_msg.qos=QOS0;
	  MQTT_msg.dup=0;
	  MQTT_msg.retained=1;
	  MQTT_msg.payload= (char *) json_buffer;
	  MQTT_msg.payloadlen=strlen( (char *) json_buffer);

	  /* Publish MQTT message */
	  if ( MQTTPublish(&c,(char*)mqtt_ibm_setup.pub_topic,&MQTT_msg) < 0) {
		  printf("\r\n [E]. Failed to publish data. Reconnecting to MQTT broker.... \r\n");
		  wifi_state=mqtt_connect;
	  }  else {
		  printf("\r\n [D]. Sensor data are published to MQTT Broker \r\n");
		  printf(MQTT_msg.payload);
		  printf("\r\n");
		 /* Blink LED2 to indicate that sensor data is published */
		 NotifyLEDBlink(500);
		 /* Bloking wait until next send */
		  HAL_Delay(MQTT_PUBLISH_DELAY-500);
	   }
	   break;

	case wifi_state_idle:
		printf(".");
		fflush(stdout);
		HAL_Delay(500);
		break;

	case wifi_state_disconnected:
		NotifyLEDOff();
		wifi_state=wifi_state_reconnect;
		break;

	default:
	  break;
	}

  }  
}

/**
  * @brief  Initialize Nucleo board
  * 
  * @retval System_Status_t (MODULE_SUCCESS/MODULE_ERROR)
  */
static System_Status_t InitNucleo(int i_usart_speed)
{
   System_Status_t status = MODULE_ERROR;
  
   /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
   HAL_Init();

   /* Configure the system clock */
   SystemClock_Config();

   /* configure the timers  */
   Timer_Config();

   UART_Configuration(115200);
   UART_Msg_Gpio_Init();
   USART_PRINT_MSG_Configuration(i_usart_speed);

   BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_GPIO);
   BSP_LED_Init(LED2);  // <hd>
   
  /* I2C Initialization */
  if(I2C_Global_Init()!=HAL_OK) {
    printf("I2C Init Error \r\n");
    return status;
  }
  else
    status = MODULE_SUCCESS;
   
   return status;
 }

/**
  * @brief  Initialize WiFi board
  * 
  * @retval System_Status_t (MODULE_SUCCESS/MODULE_ERROR)
  */
static System_Status_t ConfigWiFi(void)
{
  System_Status_t status = MODULE_ERROR;

  /* Config WiFi : disable low power mode */
  config.power=wifi_active;
  config.power_level=high;
  config.dhcp=on;//use DHCP IP address
  config.web_server=WIFI_TRUE;  
  
  status = MODULE_SUCCESS;
  return status;
}  
  
/**
  * @brief  Initialize MQTT interface
  * 
  * @retval System_Status_t (MODULE_SUCCESS/MODULE_ERROR)
  */
static System_Status_t InitMQTT(void)
{
  System_Status_t status = MODULE_ERROR;

  /* Initialize network interface for MQTT  */
  NewNetwork(&n);
  /* Initialize MQTT timers */
  status = MQTTtimer_init();
  if(status!=MODULE_SUCCESS)
  {
    printf("\r\n[E].Error in MQTTtimer_init \r\n");
    return status;
  }  

  status = MODULE_SUCCESS;
  return status;
} 


/**
  * @brief  Init timer for MQTT
  * 
  * @retval None
  */
static System_Status_t MQTTtimer_init(void){
  System_Status_t status = MODULE_ERROR;

 __TIM4_CLK_ENABLE();

  MQTTtimHandle.Instance = TIM4;
  MQTTtimHandle.Init.Period = 10-1;    //1 ms
  MQTTtimHandle.Init.Prescaler =  (uint32_t)(SystemCoreClock / 10000) - 1;
  MQTTtimHandle.Init.ClockDivision = 0;
  MQTTtimHandle.Init.CounterMode = TIM_COUNTERMODE_UP;  
  if(HAL_TIM_Base_Init(&MQTTtimHandle) != HAL_OK)
  {
    return status;
  }
  
  if (HAL_TIM_Base_Start_IT(&MQTTtimHandle) != HAL_OK)
  {
    return status;
  }

   HAL_NVIC_SetPriority(TIM4_IRQn, 0, 1);
   HAL_NVIC_EnableIRQ(TIM4_IRQn);

   status = MODULE_SUCCESS;
   return status;
}

/**
  * @brief  Initialize sensors board
  * 
  * @retval System_Status_t (MODULE_SUCCESS/MODULE_ERROR)
  */
static System_Status_t InitSensors(void)
{
   System_Status_t status = MODULE_ERROR;

    /* Force to use HTS221 */
    if(BSP_HUMIDITY_Init( HTS221_H_0, &HUMIDITY_handle )!=COMPONENT_OK){
        return status;
    }

    /* Force to use HTS221 */
    if(BSP_TEMPERATURE_Init( HTS221_T_0, &TEMPERATURE_handle )!=COMPONENT_OK){
        return status;
    }

    /* Try to use LPS25HB DIL24 if present, otherwise use LPS25HB on board */
    if(BSP_PRESSURE_Init( PRESSURE_SENSORS_AUTO, &PRESSURE_handle )!=COMPONENT_OK){
        return status;
    }
    

    /*  Enable all the sensors */
    BSP_HUMIDITY_Sensor_Enable( HUMIDITY_handle );
    BSP_TEMPERATURE_Sensor_Enable( TEMPERATURE_handle );
    BSP_PRESSURE_Sensor_Enable( PRESSURE_handle );

    status = MODULE_SUCCESS;
    return status;      
    
}



/**
* @brief  EXTI line detection callbacks
* @param  GPIO_Pin the pin connected to EXTI line
* @retval None
*/
void HAL_GPIO_EXTI_Callback( uint16_t GPIO_Pin )
{
}


/**
  * @brief  Wi-Fi callback activated when Wi-Fi is on 
  *   
  * @retval None
  */
void ind_wifi_on()
{
  printf("\r\n[D]. Wi-Fi on \r\n");
  wifi_state = wifi_state_ready;
}

/**
  * @brief  Wi-Fi callback activated when Wi-Fi is connected to AP 
  *   
  * @retval None
  */
void ind_wifi_connected()
{
  printf("\r\n [D] Connected to Wifi\r\n");
  wifi_state = mqtt_socket_create;
}

/**
  * @brief  Wi-Fi callback activated Wi-Fi is resuming from sleep mode 
  *   
  * @retval None
  */
void ind_wifi_resuming()
{
  printf("\r\n [E]. Wifi resuming from sleep user callback... \r\n");
}

/**
  * @brief  Wi-Fi callback activated in case of connection error
  *   
  * @retval None
  */
void ind_wifi_connection_error(WiFi_Status_t wifi_status)
{
  printf("\r\n [E]. WiFi connection error. Trying to reconnect to AP after some seconds ... \r\n"); 

  switch (wifi_status)
  {
     case WiFi_DE_AUTH:
         printf("[E] Error code WiFi_DE_AUTH  \r\n");        
         break;
     case WiFi_DISASSOCIATION:
         printf("[E] Error code WiFi_ DISASSOCIATION  \r\n");
         break;
     case WiFi_JOIN_FAILED:
         printf("[E] Error code WiFi_JOIN_FAILED  \r\n");
         break;
     case WiFi_SCAN_BLEWUP:
         printf("[E] Error code WiFi_ SCAN_BLEWUP \r\n");
         break;
     case WiFi_SCAN_FAILED:
         printf("[E] Error code WiFi_ SCAN_FAILED \r\n");
         break;    
     default:
         printf("[E] Undefined Error code \r\n");
         break;
  }
  
  wifi_state = wifi_state_disconnected;
}

/**
  * @brief  Wi-Fi callback activated when remote server is closed  
  * @param  socket_closed_id : socket identifier  
  * @retval None
  */
void ind_wifi_socket_client_remote_server_closed(uint8_t * socket_closed_id)
{
   printf("\r\n[E]. Remote disconnection from server. Trying to reconnect to MQTT broker... \r\n");
  // HAL_Delay (2000); 

   //spwf_socket_close()
   if (wifi_state != wifi_state_reconnect)
        wifi_state = mqtt_socket_create;
}

 /**
  * @brief  Prepare JSON packet with sensors data
  * @param  buffer : buffer that will contain sensor data in JSON format 
  * @retval None
  */
 void prepare_json_pkt (uint8_t * buffer)
{
      int32_t intPart, decPart;
      char tempbuff[40] = {'\0'};
      float PRESSURE_Value;
      float HUMIDITY_Value;
      float TEMPERATURE_Value;
             
  strcpy((char *)buffer,"{\"d\":{\"myName\":\"Nucleo\"");
  
      BSP_TEMPERATURE_Get_Temp(TEMPERATURE_handle,(float *)&TEMPERATURE_Value);
      MCR_BLUEMS_F2I_2D(TEMPERATURE_Value, intPart, decPart);
      sprintf(tempbuff, ",\"A_Temperature\":%lu.%lu",intPart, decPart);
      strcat((char *)buffer,tempbuff);
      
      BSP_HUMIDITY_Get_Hum(HUMIDITY_handle,(float *)&HUMIDITY_Value);
      MCR_BLUEMS_F2I_2D(HUMIDITY_Value, intPart, decPart);
      sprintf(tempbuff, ",\"A_Humidity\":%lu.%lu",intPart, decPart );
      strcat(  (char *)buffer,tempbuff);
      
      BSP_PRESSURE_Get_Press(PRESSURE_handle,(float *)&PRESSURE_Value);
      MCR_BLUEMS_F2I_2D(PRESSURE_Value, intPart, decPart);
      sprintf(tempbuff, ",\"A_Pressure\":%lu.%lu",intPart, decPart );
      strcat((char *)buffer,tempbuff);
      
      strcat((char *)buffer,"}}");
      
      return;
}


 /**
  * @brief  GET MAC Address from WiFi
  * @param  char* macadd : string containing MAC address
  * @retval None
  */
static System_Status_t Get_MAC_Add (char *macadd)
{
  uint8_t macaddstart[32];
  System_Status_t status = MODULE_ERROR; // 0 success
  int i,j;
       
  if (GET_Configuration_Value("nv_wifi_macaddr",(uint32_t *)macaddstart) != WiFi_MODULE_SUCCESS)
  {
      printf("Error retrieving MAC address \r\n");  
      return status; 
  }
  else
      status = MODULE_SUCCESS;
 
  macaddstart[17]='\0';
  printf("MAC orig: \r\n");  
  printf((char *)macaddstart);
  printf("\r\n");
  
  if (status == MODULE_SUCCESS)
  {  
        for(i=0,j=0;i<17;i++){
          if(macaddstart[i]!=':'){
            macadd[j]=macaddstart[i];
            j++;  
          } 
        }
        macadd[j]='\0';
  }
  
  return status;
}

/**
* @brief  Init program functionality
* @retval void
*/
int progFunctionInit(void){
  WiFi_Status_t status = WiFi_MODULE_ERROR;
  System_Status_t sysStatus = MODULE_ERROR;
  uint8_t DisplayName[32];


  /* Set WiFi AP parameters */
  printf("\r\nUsing SSID and PWD written in the code. ");
  setDefaultConfig();

  
  /* WiFi Init */
  if (ConfigWiFi () < 0 ){  
    printf("Failed to Initialize WiFi.\r\n"); 
    return 0; 
  } else {
    printf("\n\rWiFi initialized.\r\n");   
  }

  /*  Init MQTT  intf               */
  if ((sysStatus=InitMQTT())!=MODULE_SUCCESS ){  
    printf("Failed to Initialize MQTT interface.\r\n"); 
    return sysStatus; 
  } else {
    printf("MQTT initialized.\r\n");   
  }

  /* Initialize WiFi module */
  wifi_state = wifi_state_idle;  
  if((status=wifi_init(&config)) != WiFi_MODULE_SUCCESS)
  {
    printf("[E].Error in wifi init. \r\n");
    return status;
  }    
  
  /*  Get MAC Address */  
  if((sysStatus=Get_MAC_Add((char *)DisplayName)) < 0){ 
     printf("[E]. Error while retrieving MAC address \r\n");  
     return status; 
  } else {
      printf("[D]. WiFi MAC address: \r\n");
      printf((char *)DisplayName);
  }  

  /* Config MQTT Infrastructure */
  Config_MQTT ( &mqtt_ibm_setup, DisplayName );
  
 return MODULE_SUCCESS;  

}


/**
* @brief  Retrieve SysTick to increment counter
* 
* @retval tick value
*/
uint32_t user_currentTimeGetTick()
{
   return HAL_GetTick();
}  


/**
 * @brief  System Clock Configuration
 *         The system Clock is configured as follow :
 *            System Clock source            = PLL (HSI)
 *            SYSCLK(Hz)                     = 84000000
 *            HCLK(Hz)                       = 84000000
 *            AHB Prescaler                  = 1
 *            APB1 Prescaler                 = 2
 *            APB2 Prescaler                 = 1
 *            HSI Frequency(Hz)              = 16000000
 *            PLL_M                          = 16
 *            PLL_N                          = 336
 *            PLL_P                          = 4
 *            PLL_Q                          = 7
 *            VDD(V)                         = 3.3
 *            Main regulator output voltage  = Scale2 mode
 *            Flash Latency(WS)              = 2
 * 
 * @retval None
 */
void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;

  /* Enable Power Control clock */
  __PWR_CLK_ENABLE();
  
  /* The voltage scaling allows optimizing the power consumption when the device is 

     clocked below the maximum system frequency, to update the voltage scaling value 

     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);
  
  /* Enable HSI Oscillator and activate PLL with HSI as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 0x10;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 16;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);
   
  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;  
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2);
}




 /**
  * @brief  Time Handler for MQTT
  * 
  * @retval None
  */
void TIM4_IRQHandler(void)
{
   SysTickIntHandlerMQTT();   
   
   __HAL_TIM_CLEAR_FLAG(&MQTTtimHandle, TIM_FLAG_UPDATE);  
}

 /**
  * @brief  Turn on notificaion LED (LED2)
  * 
  * @retval None
  */
static void NotifyLEDOn(void)
{
 BSP_LED_On(LED2);
}

 /**
  * @brief  Turn off notificaion LED (LED2)
  * 
  * @retval None
  */
static void NotifyLEDOff(void)
{
 BSP_LED_Off(LED2);
}

 /**
  * @brief  Turn on notificaion LED (LED2)
  * @param  msPeriod time delay in milli seconds
  * @retval None
  */
static void NotifyLEDBlink(unsigned int msPeriod) 
{
   BSP_LED_Off(LED2);
   HAL_Delay(msPeriod);
   BSP_LED_On(LED2);
}

/**
  * @brief  set default values for configuration params
  * @retval None
 */
static void setDefaultConfig() {
  UnionNFCToken.configParams.qos = QOS0;
  strcpy(UnionNFCToken.configParams.NetworkSSID, ssid);
  strcpy(UnionNFCToken.configParams.NetworkKey, seckey);
  strcpy(UnionNFCToken.configParams.AuthenticationType, "WPA2");  
} 


/**
  * @brief  print HW configuration on the terminal
  * @retval None
 */
static void ShowHwConfiguration() {
    printf("\n\rHW configuration detected:\r\n");  
    printf("\tNucleo-F401RE\r\n");      
    printf("\tX-Nucleo-IDW01M1\r\n");
    printf("\tX-Nucleo-IKS01A2\r\n");    
}


/**
 * @}
 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
